const express = require('express')
const usersRoute = express.Router()
const usersMethods = require('./methods')

usersRoute.post('/create', async (req, res) => {
  const user = req.body
  usersMethods
    .create(user)
    .then(() => { res.status(200).json({ response: `Your user ${user.prenom} ${user.nom} has been inserted` }) })
    .catch(() => { res.status(500).json({ err: 'An error has occured' }) })
})


usersRoute.get('/search', (req, res) => {
  const { q } = req.query
  usersMethods
    .search(`%${q}%`)
    .then(users => { res.status(200).json({ users }) })
    .catch(() => { res.status(500).json({ err: 'An error has occured' }) })
})

usersRoute.get('/:id', (req, res) => {
  const { id } = req.params
  usersMethods
    .findUserById(id)
    .then(user => { res.status(200).json({ user }) })
    .catch(() => { res.status(500).json({ err: 'This user is not existing' }) })
})

usersRoute.put('/edit/:id', (req, res) => {
  const { id } = req.params
  const userInfos = []
  usersMethods
    .findUserById(id)
    .then(() => {
      Object.keys(req.body).map(key => {
        userInfos.push({ [key]: req.body[key] })
      })
      usersMethods
        .update(userInfos, id)
        .then(() => { res.status(200).json({ response: `The user has been updated` }) })
        .catch((e) => { res.status(500).json({ err: 'An error has occured' }) })
    })
    .catch(() => { res.status(500).json({ err: 'This user is not existing' }) })
})

usersRoute.delete('/delete/:id', (req, res) => {
  const { id } = req.params
  usersMethods
    .delete(id)
    .then(() => { res.status(200).json({ response: `Your user with the id ${id} has been deleted` }) })
    .catch(() => { res.status(500).json({ err: 'An error has occured' }) })
})

module.exports = usersRoute
