const connexion = require('../models/db')

const usersMethods = {
  async findUserById(id) {
    const queryString = `
      SELECT nom, prenom, telephone, adresse, ville
      FROM users
      WHERE id = ?
    `
    return await connexion.query(queryString, [id])
  },
  async create(user) {
    const {
      nom,
      prenom,
      telephone,
      adresse,
      ville
    } = user
    const queryString = `
      INSERT INTO users (nom, prenom, telephone, adresse, ville)
      VALUES (?, ?, ?, ?, ?)
    `
    return await connexion.query(queryString, [nom, prenom, telephone, adresse, ville])
  },
  async update(user, id) {
    let queryString = `
      UPDATE users
      SET
    `
    user.map((key, i) => {
      for (attr in key) {
        queryString += `${attr} = "${key[attr]}"${i === user.length - 1 ? '' : ','} `
      }
    })
    queryString += `WHERE id = ?`
    return await connexion.query(queryString, [id])
  },
  async delete(id) {
    const queryString = `
      DELETE FROM users
      WHERE id = ?
    `
    return await connexion.query(queryString, [id])
  },
  async search(query) {
    const queryString = `
      SELECT nom, prenom, telephone, adresse, ville
      FROM users
      WHERE prenom like ? OR nom like ?
    `
    return await connexion.query(queryString, [query, query])
  }
}

module.exports = usersMethods
