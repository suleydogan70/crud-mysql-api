/* --- Dependencies --- */
const express = require('express')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const app = express()
const usersRoutes = require('./controllers/routes')
const connexion = require('./models/db.js')

/* --- MiddleWares --- */
app.use(bodyParser.urlencoded({extended: true}))
app.use(helmet())
app.use('/users', usersRoutes)

/* --- Exit event handler --- */
process.on('exit', (code) => {
  console.log(`About to exit with code ${code}`)
  connexion.end()
})

/* --- Server --- */
app.listen(8080)
