const mysql = require('mysql')
const util = require('util')
const identification = require('./identification')

const connexion = mysql.createPool({
  connectionLimit: 10,
  host: identification.host,
  user: identification.user,
  password: identification.password,
  database: identification.database
})
connexion.query = util.promisify(connexion.query)

module.exports = connexion
